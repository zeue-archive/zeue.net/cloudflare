#!/usr/bin/env node

const inquirer = require("inquirer");
const git = require("simple-git");
const { exec } = require("child_process");

let questions = [
  {
    type: "list",
    name: "addall",
    message: "Have you added all changed files?",
    choices: ["yes, continue with committing", "no, add them all for me (git add -A)"]
  },
  {
    type: "list",
    name: "type",
    message: "Type:",
    choices: ["added", "changed", "deprecated", "removed", "fixed", "security"]
  },
  {
    type: "list",
    name: "scope",
    message: "Scope:",
    choices: ["core", "docs", "meta"]
  },
  {
    type: "input",
    name: "subject",
    message: "Commit message:"
  }
];

inquirer.prompt(questions).then(data => {
  if (data["addall"] === "no, add them all for me (git add -A)") {
    exec("git add -A", (err, stdout, stderr) => {
      if (err) return;
      console.log(`adding all changed files now...`);
      console.log(`finished adding all changed files!`);
      git().commit(data["type"] + "(" + data["scope"] + "): " + data["subject"]);
    });
  } else {
    git().commit(data["type"] + "(" + data["scope"] + "): " + data["subject"]);
  }
});
