#!/bin/bash

if [[ -z $1 ]]; then
  echo "gce job not set!"
  exit 2
fi

if [[ $1 = "update" ]]; then
  if [[ -z $2 ]]; then
    echo "target workload name must be set!"
    exit 2
  fi
  if [[ -z $3 ]]; then
    echo "target update version must be set!"
    exit 2
  fi
  if [[ $3 = "latest" ]]; then
    targetVersion=$(git describe --abbrev=0 --tags)
  else
    targetVersion=$(git tag --list "$3-b*")
  fi
  gcloud container clusters get-credentials CLUSTER --zone REGION --project PROJECT
  kubectl --namespace=default set image deployment $2-example-com PROJECT=gcr.io/PROJECT/REPO/$targetVersion:latest && echo "$targetVersion has been deployed to $2-example-com"
fi
