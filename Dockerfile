FROM node:8.12.0

COPY . ./cf.zeue.net

RUN cd cf.zeue.net && npm i

EXPOSE 80/tcp

CMD cd cf.zeue.net && npm start
